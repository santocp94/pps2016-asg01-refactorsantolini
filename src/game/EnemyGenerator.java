package game;

import characters.AbstractEnemy;
import characters.Character;
import characters.EnemyFactory;
import characters.EnemyFactoryImpl;

import java.util.Random;

/**
 * Created by nicola on 16/03/17.
 */
public class EnemyGenerator {

    private static final int MAX_ENEMY_POSITION = 3500;
    private static final int ENEMY_POSITION_OFFSET = 800;

    private EnemyFactory enemyFactory;
    private Random random = new Random();

    public EnemyGenerator(){
        this.enemyFactory = new EnemyFactoryImpl();
    }

    //now unused, but could be usefull
    public Character generateTurtle(){
        return enemyFactory.createTurtle(random.nextInt(MAX_ENEMY_POSITION) + ENEMY_POSITION_OFFSET);
    }

    public Character generateMushroom(){
        return enemyFactory.createMushroom(random.nextInt(MAX_ENEMY_POSITION) + ENEMY_POSITION_OFFSET);
    }

    public Character generateRandomEnemy(){
        return (random.nextDouble() > 0.5) ? enemyFactory.createMushroom(random.nextInt(MAX_ENEMY_POSITION) + ENEMY_POSITION_OFFSET) : enemyFactory.createTurtle(random.nextInt(4000));
    }

}
