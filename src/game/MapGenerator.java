package game;

import objects.Coin;
import objects.GameObject;
import objects.GameObjectFactory;
import objects.GameObjectFactoryImpl;

import java.util.ArrayList;

/**
 * Created by nicola on 16/03/17.
 */
public class MapGenerator {

    private GameObjectFactory objectFactory;

    public MapGenerator(){
        this.objectFactory = new GameObjectFactoryImpl();
    }

    public ArrayList<GameObject> generateBlocksAndTunnels(){
        ArrayList<GameObject> objects = new ArrayList<>();

        //with more time, should read this from a file
        objects.add(objectFactory.createTunnel(500, 230));
        objects.add(objectFactory.createTunnel(1000, 230));
        objects.add(objectFactory.createTunnel(1600, 230));
        objects.add(objectFactory.createTunnel(1900, 230));
        objects.add(objectFactory.createTunnel(2500, 230));
        objects.add(objectFactory.createTunnel(3000, 230));
        objects.add(objectFactory.createTunnel(3800, 230));
        objects.add(objectFactory.createTunnel(4500, 230));

        objects.add(objectFactory.createBlock(400, 180));
        objects.add(objectFactory.createBlock(1200, 180));
        objects.add(objectFactory.createBlock(1270, 170));
        objects.add(objectFactory.createBlock(1340, 160));
        objects.add(objectFactory.createBlock(2000, 180));
        objects.add(objectFactory.createBlock(2600, 160));
        objects.add(objectFactory.createBlock(2650, 180));
        objects.add(objectFactory.createBlock(3500, 160));
        objects.add(objectFactory.createBlock(3550, 140));
        objects.add(objectFactory.createBlock(4000, 170));
        objects.add(objectFactory.createBlock(4200, 200));
        objects.add(objectFactory.createBlock(4300, 210));
        return objects;
    }

    public ArrayList<Coin> generateCoins(){
        ArrayList<Coin> coins = new ArrayList<>();

        coins.add(objectFactory.createCoin(402, 145));
        coins.add(objectFactory.createCoin(1202, 140));
        coins.add(objectFactory.createCoin(1272, 95));
        coins.add(objectFactory.createCoin(1342, 40));
        coins.add(objectFactory.createCoin(1650, 145));
        coins.add(objectFactory.createCoin(2650, 145));
        coins.add(objectFactory.createCoin(3000, 145));
        coins.add(objectFactory.createCoin(3400, 135));
        coins.add(objectFactory.createCoin(4200, 145));
        coins.add(objectFactory.createCoin(4600, 40));
        return coins;
    }

}
