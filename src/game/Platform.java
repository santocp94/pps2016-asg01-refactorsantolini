package game;

import characters.Character;
import objects.*;
import characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final int ENEMY_NUMBER = 4;
    private static final int MAP_BOUND = 4600;
    private static final int BACKGROUND_SWITCH = 800;
    private static final int FLOOR_OFFSET = 293;
    private static final int STARTING_X_MARIO = 300;
    private static final int STARTING_Y_MARIO = 245;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    public Mario mario;
    private ArrayList<Character> enemies = new ArrayList<>();

    private Image imgFlag;
    private Image imgCastle;

    private ArrayList<GameObject> objects = new ArrayList<>();
    private ArrayList<Coin> coins = new ArrayList<>();

    public Platform() {
        super();
        initializeScreen();

        mario = new Mario(STARTING_X_MARIO, STARTING_Y_MARIO);
        EnemyGenerator enemyGenerator = new EnemyGenerator();
        for(int i = 0; i < ENEMY_NUMBER; i++){
            enemies.add(enemyGenerator.generateRandomEnemy());
        }

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        MapGenerator mapGenerator = new MapGenerator();
        objects = mapGenerator.generateBlocksAndTunnels();
        coins = mapGenerator.generateCoins();

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
    }

    private void initializeScreen(){
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = FLOOR_OFFSET;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= MAP_BOUND) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -BACKGROUND_SWITCH) {
            this.background1PosX = BACKGROUND_SWITCH;
        }
        else if (this.background2PosX == -BACKGROUND_SWITCH) {
            this.background2PosX = BACKGROUND_SWITCH;
        }
        else if (this.background1PosX == BACKGROUND_SWITCH) {
            this.background1PosX = -BACKGROUND_SWITCH;
        }
        else if (this.background2PosX == BACKGROUND_SWITCH) {
            this.background2PosX = -BACKGROUND_SWITCH;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (GameObject obj : objects) {
            if (this.mario.isNearby(obj))
                this.mario.contact(obj);

            for(Character e : enemies){
                if(e.isNearby(obj)){
                    e.contact(obj);
                }
            }
        }

        for (int i = 0; i < coins.size(); i++) {
            if (this.mario.contactPiece(this.coins.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.coins.remove(i);
            }
        }

        for(Character e : enemies){
            if(this.mario.isNearby(e)){
                this.mario.contact(e);
            }
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= MAP_BOUND) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < coins.size(); i++) {
                this.coins.get(i).move();
            }

            for(Character e : enemies) {
                e.move();
            }
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).getImgObj(), this.objects.get(i).getGameObjectX(),
                    this.objects.get(i).getGameObjectY(), null);
        }

        for (int i = 0; i < coins.size(); i++) {
            g2.drawImage(this.coins.get(i).imageOnMovement(), this.coins.get(i).getGameObjectX(),
                    this.coins.get(i).getGameObjectY(), null);
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g2.drawImage(this.mario.walk(), this.mario.getX(), this.mario.getY(), null);

        for(Character e : enemies){
            if(e.isAlive()) {
                g2.drawImage(e.walk(), e.getX(), e.getY(), null);
            } else {
                g2.drawImage(e.deadImage(), e.getX(), e.getY() + 25, null);
            }
        }

    }
}
