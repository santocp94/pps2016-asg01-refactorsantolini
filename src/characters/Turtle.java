package characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Turtle extends AbstractEnemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int TURTLE_Y = 243;
    private static final int TURTLE_FREQUENCY = 45;
    private static final String TURTLE_NAME = Res.IMGP_CHARACTER_TURTLE;

    private Image img;
    private int offsetX;

    Turtle(int X) {
        super(X, TURTLE_Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);
        this.offsetX = 1;
        this.img = Utils.getImage(Res.IMG_TURTLE_IDLE);

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    @Override
    protected String doWalk() {
        incCounter();
        return Res.IMG_BASE + TURTLE_NAME + (!super.isMoving() || super.getCounter() % TURTLE_FREQUENCY == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (super.isToRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
    }

    @Override
    public void contact(Character pers) {

        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

}
