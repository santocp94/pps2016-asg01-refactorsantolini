package characters;

import java.awt.Image;

import game.Main;
import objects.AbstractGameObject;
import objects.GameObject;
import utils.Res;
import utils.Utils;

public abstract class AbstractCharacter implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private int x, y;
    private boolean moving;
    private boolean toRight;
    private int counter;
    protected boolean alive;

    public AbstractCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void incCounter(){this.counter++;}

    public Image walk() {
        return Utils.getImage(doWalk());
    }

    protected abstract String doWalk();

    public void move() {
        if (Main.scene.getxPos() >= 0) {
            this.x = this.x - Main.scene.getMov();
        }
    }

    public boolean hitAhead(GameObject og) {
        if (this.x + this.width < og.getGameObjectX() || this.x + this.width > og.getGameObjectX() + 5 ||
                this.y + this.height <= og.getGameObjectY() || this.y >= og.getGameObjectY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBack(GameObject og) {
        if (this.x > og.getGameObjectX() + og.getWidth() || this.x + this.width < og.getGameObjectX() + og.getWidth() - 5 ||
                this.y + this.height <= og.getGameObjectY() || this.y >= og.getGameObjectY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBelow(GameObject og) {
        if (this.x + this.width < og.getGameObjectX() + 5 || this.x > og.getGameObjectX() + og.getWidth() - 5 ||
                this.y + this.height < og.getGameObjectY() || this.y + this.height > og.getGameObjectY() + 5) {
            return false;
        } else
            return true;
    }

    protected boolean hitAbove(GameObject og) {
        if (this.x + this.width < og.getGameObjectX() + 5 || this.x > og.getGameObjectX() + og.getWidth() - 5 ||
                this.y < og.getGameObjectY() + og.getHeight() || this.y > og.getGameObjectY() + og.getHeight() + 5) {
            return false;
        } else return true;
    }

    protected boolean hitAhead(Character pers) {
        if (this.isToRight()) {
            if (this.x + this.width < pers.getX() || this.x + this.width > pers.getX() + 5 ||
                    this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    protected boolean hitBack(Character pers) {
        if (this.x > pers.getX() + pers.getWidth() || this.x + this.width < pers.getX() + pers.getWidth() - 5 ||
                this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight())
            return false;
        return true;
    }

    public boolean hitBelow(Character pers) {
        if (this.x + this.width < pers.getX() || this.x > pers.getX() + pers.getWidth() ||
                this.y + this.height < pers.getY() || this.y + this.height > pers.getY())
            return false;
        return true;
    }

    public boolean isNearby(Character pers) {
        if ((this.x > pers.getX() - PROXIMITY_MARGIN && this.x < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > pers.getX() - PROXIMITY_MARGIN && this.x + this.width < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }

    public boolean isNearby(GameObject obj) {
        if ((this.x > obj.getGameObjectX() - PROXIMITY_MARGIN && this.x < obj.getGameObjectX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getGameObjectX() - PROXIMITY_MARGIN && this.x + this.width < obj.getGameObjectX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
