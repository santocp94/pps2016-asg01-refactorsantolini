package characters;

import objects.GameObject;

import java.awt.Image;

public interface Character {
    Image walk();

    boolean isNearby(Character pers);

    boolean isNearby(GameObject obj);

    void contact(Character pers);

    void contact(GameObject obj);

    void move();

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    boolean isAlive();

    Image deadImage();

    void setMoving(boolean moving);

    void setAlive(boolean alive);
}
