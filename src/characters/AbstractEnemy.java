package characters;

import objects.GameObject;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by nicola on 15/03/17.
 */
public abstract class AbstractEnemy extends AbstractCharacter implements Runnable{

    private final int PAUSE = 15;
    private int offsetX;

    public AbstractEnemy(int x, int y, int width, int height) {
        super(x, y, width, height);
    }


    public Image walk() {
        return Utils.getImage(doWalk());
    }

    protected abstract String doWalk();

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        super.setX(super.getX() + this.offsetX);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public void contact(Character pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
