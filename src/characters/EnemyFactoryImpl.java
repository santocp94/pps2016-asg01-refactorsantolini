package characters;

/**
 * Created by nicola on 15/03/17.
 */
public final class EnemyFactoryImpl implements EnemyFactory{

    @Override
    public AbstractEnemy createMushroom(int x) { return new Mushroom(x);
    }

    @Override
    public AbstractEnemy createTurtle(int x) {
        return new Turtle(x);
    }
}
