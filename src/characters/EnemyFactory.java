package characters;

/**
 * Created by nicola on 15/03/17.
 */
public interface EnemyFactory {

    Character createMushroom(int x);

    Character createTurtle(int x);
}
