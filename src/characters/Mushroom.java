package characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Mushroom extends AbstractEnemy {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int MUSHROOM_Y = 263;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final String MUSHROOM_NAME = Res.IMGP_CHARACTER_MUSHROOM;

    private Image img;
    private int offsetX;

    Mushroom(int x) {
        super(x, MUSHROOM_Y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;
        this.img = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    @Override
    protected String doWalk() {
        incCounter();
        return Res.IMG_BASE + MUSHROOM_NAME + (!super.isMoving() || super.getCounter() % MUSHROOM_FREQUENCY == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (super.isToRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
    }

    @Override
    public void contact(Character pers) {

        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

}
