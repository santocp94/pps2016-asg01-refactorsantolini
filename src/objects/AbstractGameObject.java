package objects;

import java.awt.Image;

import game.Main;

public abstract class AbstractGameObject implements GameObject{

    private int width, height;
    private int x, y;

    protected Image imgObj;

    public AbstractGameObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getGameObjectX() {
        return x;
    }

    public int getGameObjectY() {
        return y;
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void move() {
        if (Main.scene.getxPos() >= 0) {
            this.x = this.x - Main.scene.getMov();
        }
    }

}
