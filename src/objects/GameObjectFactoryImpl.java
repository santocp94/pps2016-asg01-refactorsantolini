package objects;

/**
 * Created by nicola on 15/03/17.
 */
public class GameObjectFactoryImpl implements GameObjectFactory {
    @Override
    public GameObject createBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public Coin createCoin(int x, int y) {
        return new Coin(x, y);
    }

    @Override
    public GameObject createTunnel(int x, int y) {
        return new Tunnel(x, y);
    }
}
